#!/usr/bin/env python

import os
import rospkg
import shutil
import xml.etree.ElementTree as ET

package_path = rospkg.RosPack().get_path('robologic_gazebo')
image_directory = os.path.join(package_path, 'markers')
template_directory = os.path.join(package_path, 'models', 'robologic',
    'marker_template')
output_directory = os.path.join(package_path, 'models', 'robologic', 'markers')

if os.path.exists(output_directory):
  shutil.rmtree(output_directory)
os.mkdir(output_directory)

for filename in os.listdir(image_directory):
  model_name, _ = os.path.splitext(filename)
  model_directory = os.path.join(output_directory, model_name)
  shutil.copytree(template_directory, model_directory)
  shutil.copy(os.path.join(image_directory, filename), model_directory)

  sdf_filename = os.path.join(model_directory, 'model.sdf')
  xml = ET.parse(sdf_filename)
  material_script = xml.find('./model/link/visual/material/script')
  material_uri = material_script.find('./uri')
  material_name = material_script.find('./name')
  material_uri.text = material_uri.text.replace('marker_template',
      os.path.join('markers', model_name))
  material_name.text = model_name
  xml.write(sdf_filename)

  material_filename = os.path.join(model_directory, 'marker.material')
  material_content = open(material_filename).read()
  open(material_filename, 'w').write(material_content.replace(
      'marker_template', model_name))
